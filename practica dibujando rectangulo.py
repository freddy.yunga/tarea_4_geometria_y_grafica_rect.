
import pygame
import time
#inicia pygame
pygame.init()
#medidas ventana
ancho=800
largo=600
#colores
blanco=255,255, 255
negro=0,0,0
rojo=255,0,0
verde=0,255,0
azul=0,0,255


# Cambio el título de la ventana
pygame.display.set_caption(" RECTANGULO")
# ventana
ventana = pygame.display.set_mode((ancho, largo))
# dibujo
ventana.fill(rojo)
pygame.draw.rect(ventana,verde,(2,3,200,300))

# actualizar
pygame.display.update()
#cerrar ventana
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
# salir
#time.sleep(10)
pygame.quit()